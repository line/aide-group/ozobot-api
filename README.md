# Ozobot Evo control REST API

This project provides an HTTP server exposing services for controlling Ozobo Evo robots
remotely, using their Bluetooth connection.

It is based on the [Ozobot Evo control library](https://gitlab.inria.fr/line/aide-group/ozobot-lib).

## Build

Use the `make wheel` _(or `make` alone, since `wheel` is the default target of the Makefile)_. 

This will produce the deployment wheel in the `build` sub-directory of the project 
_(automatically created if not yet there)_.

## Installation

Installation is achieved with the usual `pip` command :
```bash
$ pip install path/to/ozoevo_api-x.x.x-py3-none-any.whl
```

This will install the server package and convenience management commands. Dependencies
are automatically downloaded and installed if needed. The target system must thus be
connected to the Internet.

To relieve from having to build the wheels from sources, the pre-built one for the latest 
released versions is available for download in the `dist` sub-directory of the repository.

Previous versions are archived in the `dist-arch` sub-directory. 

## Manual execution

The server is started by the command `ozoevo-srvd` command. When started this way, it runs
in foreground mode, and can be stopped by hitting `<Ctrl-C>`.

It listens by default on port `8000`.

## System service

**Warning**

> This section applies only on Linux systems. In addition, it is based on `systemd` and thus
is valid on distributions using it (Debian and its derivative for instance, including Raspbian)

The command `ozoevo-install-service` installs and configures a `systemd` service, which is 
started automatically at boot time, once the network is up and running.

The service can then be controlled with the standard `systemd` commands:

**status display :**
```bash
$ systemctl status ozoevo-srvd.service 
● ozoevo-srvd.service - REST API server for controlling an OzoBot Evo
   Loaded: loaded (/lib/systemd/system/ozoevo-srvd.service; enabled; vendor preset: enabled)
  Drop-In: /etc/systemd/system/ozoevo-srvd.service.d
           └─override.conf
   Active: active (running) since Fri 2019-08-02 00:27:21 CEST; 1 day 11h ago
 Main PID: 322 (ozoevo-srvd)
    Tasks: 5 (limit: 2200)
   Memory: 26.7M
   CGroup: /system.slice/ozoevo-srvd.service
           └─322 /usr/bin/python3 /usr/local/bin/ozoevo-srvd

... (last lines of the log here) ... 
```  

**stop :**
```bash
$ sudo systemctl stop ozoevo-srvd.service
```

**start :**
```bash
$ sudo systemctl start ozoevo-srvd.service
```

**restart :**
```bash
$ sudo systemctl restart ozoevo-srvd.service
```    

**log display :**
```bash
$ journalctl -u ozoevo-srvd.service
-- Logs begin at Thu 2019-02-14 11:11:59 CET, end at Sat 2019-08-03 12:24:39 CEST. --
Aug 03 12:24:40 rpi3b-desktop ozoevo-srvd[1313]: 12:24:40 [I] uvicorn              > Started server process [1313]
Aug 03 12:24:40 rpi3b-desktop ozoevo-srvd[1313]: 12:24:40 [I] uvicorn              > Waiting for application startup.
Aug 03 12:24:40 rpi3b-desktop ozoevo-srvd[1313]: 12:24:40 [I] app                  > --------------------------------------------------------------------------------
Aug 03 12:24:40 rpi3b-desktop ozoevo-srvd[1313]: 12:24:40 [I] app                  > starting application...
Aug 03 12:24:40 rpi3b-desktop ozoevo-srvd[1313]: 12:24:40 [I] app                  > Version : 0.1.2
Aug 03 12:24:40 rpi3b-desktop ozoevo-srvd[1313]: 12:24:40 [I] app                  > Settings:
Aug 03 12:24:40 rpi3b-desktop ozoevo-srvd[1313]: 12:24:40 [I] app                  > - DEBUG                : False
Aug 03 12:24:40 rpi3b-desktop ozoevo-srvd[1313]: 12:24:40 [I] app                  > - OFFLINE_MODE         : False
Aug 03 12:24:40 rpi3b-desktop ozoevo-srvd[1313]: 12:24:40 [I] app                  > - SPEED_FACTOR         : 1
Aug 03 12:24:40 rpi3b-desktop ozoevo-srvd[1313]: 12:24:40 [I] app                  > - SPEED_MAX            : 3000
Aug 03 12:24:40 rpi3b-desktop ozoevo-srvd[1313]: 12:24:40 [I] app                  > - SCAN_TIMEOUT         : 5
Aug 03 12:24:40 rpi3b-desktop ozoevo-srvd[1313]: 12:24:40 [I] app                  > registry initialization started
Aug 03 12:24:40 rpi3b-desktop ozoevo-srvd[1313]: 12:24:40 [I] app.registry         > creating registry instance
Aug 03 12:24:40 rpi3b-desktop ozoevo-srvd[1313]: 12:24:40 [I] app.registry         > refreshing robot directory...
Aug 03 12:24:45 rpi3b-desktop ozoevo-srvd[1313]: 12:24:45 [I] app.registry         > - 0 MAC address change(s)
Aug 03 12:24:45 rpi3b-desktop ozoevo-srvd[1313]: 12:24:45 [I] app.registry         > - 1 new bot(s) discovered
Aug 03 12:24:45 rpi3b-desktop ozoevo-srvd[1313]: 12:24:45 [I] app.registry         > - 0 bot(s) gone
Aug 03 12:24:45 rpi3b-desktop ozoevo-srvd[1313]: 12:24:45 [I] app.registry         > registry contains 1 bot(s) : DarkEvo
Aug 03 12:24:45 rpi3b-desktop ozoevo-srvd[1313]: 12:24:45 [I] app                  > registry initialization complete
Aug 03 12:24:45 rpi3b-desktop ozoevo-srvd[1313]: 12:24:45 [I] app                  > startup sequence complete
Aug 03 12:24:45 rpi3b-desktop ozoevo-srvd[1313]: 12:24:45 [I] app                  > --------------------------------------------------------------------------------
Aug 03 12:24:45 rpi3b-desktop ozoevo-srvd[1313]: 12:24:45 [I] uvicorn              > Uvicorn running on http://0.0.0.0:8000 (Press CTRL+C to quit)
. . . 

```

## Configuration

The server is configured by the following `OZOEVO_API_xxx` environment variables :

<table>
<tr><th>Name</th><th>Default</th><th>Description</th></tr>
<tr>
    <td>OZOEVO_API_DEBUG</td>
    <td>0</td>
    <td>
        If set to "1" (or "yes", "true" or "on", no matter the case), 
        activates debug features.
    </td>
</tr>
<tr>
    <td>OZOEVO_API_LISTENING_PORT</td>
    <td>8000</td>
    <td>
        The TCP port the server process listens on.
    </td>
</tr>
<tr>
    <td>OZOEVO_API_OFFLINE_MODE</td>
    <td>0</td>
    <td>
        If set to "1" (or "yes", "true" or "on", no matter the case),
        disables real Bluetooth communication and stores sent data in a buffer.
        This option is handy for testing the server itself without needing physical
        robots.   
    </td>
</tr>
<tr>
    <td>OZOEVO_API_SCAN_TIMEOUT</td>
    <td>5</td>
    <td>
        The number of seconds the device discovery scan will wait before terminating. 
    </td>
</tr>
<tr>
    <td>OZOEVO_API_SPEED_FACTOR</td>
    <td>1</td>
    <td>
        The factor applied to speed values. It can be set to any float value, in order 
        to allow the client applications to express speeds in domain units (e.g. cm/sec).  
    </td>
</tr>
<tr>
    <td>OZOEVO_API_SPEED_MAX</td>
    <td>3000</td>
    <td>
        The upper limit of the speed parameters in requests. The value is expressed
        in raw robot units.  
    </td>
</tr>
</table>

The default values are automatically set in the service descriptor, but they can be 
overridden without having to modify the file. The command `sudo systemctl edit ozoevo-srvd.service`
will opens the default editor in which it is possible to define defaults overrides. 

**Note :** 
>This file is not en environment file, but a service descriptor overlay, which lines
will be merged into the original descriptor. Its syntax thus follows `systemd` 
service descriptors one. 

Example :
```ini
[Service]
Environment="OZOEVO_SCAN_TIMEOUT=3"
Environment="OZOEVO_SPEED_MAX=2000"
```

## API documentation

The API includes routes for the following operations:
- robots discovery
- robot moves control
- robot LEDs control

Their documentation is provided as a browsable API, based on Swagger UI. It is located
at `http://<host>:<port>/docs`.

Here are a couple of screenshots:

- Routes list :
![Routes list](./README.d/routes.png "Routes list")
- Route request detail :
![Route request detail](./README.d/route_request_detail.png "Route request detail")
- Route responses detail :
![Route responses detail](./README.d/route_responses_detail.png "Route responses detail")
- Route tryout :
![Route tryout](./README.d/route_tryout.png "Route tryout")
- API schemas :
![API schemas](./README.d/api_schemas.png "API schemas")

