REMOTE?=rpi-desktop

wheel:
	mkdir -p dist dist-arch
	if [ -e dist/* ] ; then mv dist/* dist-arch/ ; fi
	python setup.py bdist_wheel

install:
	pip install dist/*.whl

tests:
	PYTHONPATH=. pytest tests

clean:
	rm -rf build dist

runserver:
	uvicorn ozoevo_api.main:app --reload

deploy:
	scp dist/*.whl $(REMOTE):

.PHONY: tests clean install wheel runserver deploy
