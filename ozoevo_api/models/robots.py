from typing import List

from pydantic import BaseModel, constr, conint, confloat, validator, Schema

from ozoevo.robot import LedSelector

from ozoevo_api.settings import SPEED_MAX

mac_addr_re = '^[0-9a-fA-F]{2}(:[0-9a-fA-F]{2}){5}$'


class BTDevice(BaseModel):
    """ Bluetooth device information
    """
    mac_addr: constr(regex=mac_addr_re) = Schema(
        ...,
        description='device MAC address, as advertised during the scan'
    )
    device_name: str = Schema(
        ...,
        description='device name, as advertised during the scan'
    )


class RobotDescriptor(BaseModel):
    """ Descriptor of an Ozo Evo robot, from the Bluetooth perspective
    """
    name: str = Schema(
        ...,
        description="robot name, as set on the application"
    )
    device: BTDevice = Schema(
        ...,
        description="Bluetooth device information"
    )


class RGB(BaseModel):
    """ Color specification using the RGB model.
    """
    red: conint(ge=0, le=255) = Schema(
        default=0,
        title='red component of the color'
    )
    green: conint(ge=0, le=255) = Schema(
        default=0,
        title='green component of the color'
    )
    blue: conint(ge=0, le=255) = Schema(
        default=0,
        title='blue component of the color'
    )


VALID_ALL_LEDS_SYMBOLS = {'all', '*'}


class LEDsSettings(BaseModel):
    """ Settings (selection and color) of the robot LEDs.
    """
    leds: List[str] = Schema(
        default=[LedSelector.top.name],
        description="the list of LEDs to be set"
    )
    color: RGB = Schema(
        default=...,
        description='the color to be applied to listed LEDs'
    )

    @validator('leds')
    def check_led_name(cls, v):
        if not v:
            raise ValueError(f"empty LED name found in set")
        try:
            LedSelector[v]
        except KeyError:
            raise ValueError(f"invalid LED name: {v}")

        return v

    @validator('leds', pre=True, whole=True)
    def check_leds_set(cls, v):
        if not v:
            raise ValueError("the LED set cannot be empty")

        if len(v) == 1:
            if v[0].lower() in VALID_ALL_LEDS_SYMBOLS:
                return [l.name for l in LedSelector]

        else:
            if VALID_ALL_LEDS_SYMBOLS & set(v):
                raise ValueError(f"{VALID_ALL_LEDS_SYMBOLS} symbols must be used alone")

        return v


class DriveSettings(BaseModel):
    """ Settings for motors activation.
    """
    left_speed: conint(ge=-SPEED_MAX, le=SPEED_MAX) = Schema(
        default=0,
        description="left motor speed (in xxx/s)"
    )
    right_speed: conint(ge=-SPEED_MAX, le=SPEED_MAX) = Schema(
        default=0,
        description="right motor speed (in xxx/s)"
    )
    delay: confloat(ge=0) = Schema(
        default=1,
        description="delay (in seconds) after which the motors will be automatically turned off"
    )
    wait: bool = Schema(
        default=True,
        description="should we wait for the move to end ?"
    )

    @validator('delay')
    def check_delay(cls, v, values):
        if v == 0 and values and (values['left_speed'] or values['right_speed']):
            raise ValueError('delay cannot be 0 if any motor speed is not null')

        return v


class SpinSettings(BaseModel):
    """ Settings for motors activation.
    """
    speed: conint(ge=-SPEED_MAX, le=SPEED_MAX) = Schema(
        default=0,
        description="left and right motor speed (in xxx/s)"
    )
    delay: confloat(ge=0) = Schema(
        default=1,
        description="delay (in seconds) after which the motors will be automatically turned off"
    )
    wait: bool = Schema(
        default=True,
        description="should we wait for the move to end ?"
    )

    @validator('delay')
    def check_delay(cls, v, values):
        if v == 0 and values and values['speed']:
            raise ValueError('delay cannot be 0 if any motor speed is not null')

        return v
