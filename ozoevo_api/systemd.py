import os
import sys
import pkg_resources
import subprocess
import shutil

from colorama import Fore, Back, Style


class SystemdCommand:
    SERVICE_NAME = 'ozoevo-srvd'

    title = ''

    def execute(self):
        if os.getuid() != 0:
            sys.exit('Aborted : command requires root privileges')

        print(f'{Style.BRIGHT}{Fore.WHITE}{self.title}...{Style.NORMAL}')
        try:
            self.do_execute()
        except Exception as e:
            print(f'{Fore.RED}Unexpected error:\n{e}{Style.RESET_ALL}')
        else:
            print(f'\n{Style.BRIGHT}{Fore.GREEN}Success.{Style.RESET_ALL}\n')

    def progress(self, msg):
        print(f'{Fore.BLUE}- {msg}...{Style.RESET_ALL}')

    def do_execute(self):
        raise NotImplementedError()


class InstallService(SystemdCommand):
    title = f'Configuring service {SystemdCommand.SERVICE_NAME}'

    def do_execute(self):
        self.progress('stopping service')
        subprocess.run(['systemctl', 'stop', self.SERVICE_NAME])

        self.progress('installing service descriptor')
        resource_package = 'ozoevo_api'
        resource_path = f'data/{self.SERVICE_NAME}.service'
        shutil.copy(pkg_resources.resource_filename(resource_package, resource_path), '/lib/systemd/system/')

        self.progress('enabling service')
        subprocess.run(['systemctl', 'enable', self.SERVICE_NAME])
        self.progress('starting service')
        subprocess.run(['systemctl', 'start', self.SERVICE_NAME])
        print()
        print(f'{Style.BRIGHT}{Fore.WHITE}Service status check :{Style.NORMAL}')
        subprocess.run(['systemctl', 'status', self.SERVICE_NAME])


class UninstallService(SystemdCommand):
    title = f'Removing service {SystemdCommand.SERVICE_NAME}'

    def do_execute(self):
        self.progress('stopping service')
        subprocess.run(['systemctl', 'stop', self.SERVICE_NAME])
        self.progress('disabling service')
        subprocess.run(['systemctl', 'disable', self.SERVICE_NAME])
        self.progress('removing service descriptor')
        os.remove(f'/lib/systemd/system/{self.SERVICE_NAME}.service')


class RestartService(SystemdCommand):
    title = f'Restarting service {SystemdCommand.SERVICE_NAME}'

    def do_execute(self):
        subprocess.run(['systemctl', 'restart', self.SERVICE_NAME])
        print(f'{Style.BRIGHT}{Fore.WHITE}Service status check :{Style.NORMAL}')
        subprocess.run(['systemctl', 'status', self.SERVICE_NAME])


def install_service():
    InstallService().execute()


def uninstall_service():
    UninstallService().execute()


def restart_service():
    RestartService().execute()
