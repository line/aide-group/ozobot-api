import os
import sys

from fastapi import FastAPI

from ozoevo_api.api.api import api_router
from ozoevo_api.log import app_log
import ozoevo_api.settings
from ozoevo_api.version import VERSION

app = FastAPI(
    title="Ozobot EVO robot control API",
    version=VERSION,
    redoc_url=None,
)


app.include_router(api_router, prefix='/api')


@app.on_event('startup', )
def banner_open():
    app_log.info('-' * 80)
    app_log.info("starting application...")
    app_log.info("Version : %s", app.version)
    app_log.info("Settings:")
    for var_name in ozoevo_api.settings.__all__:
        app_log.info("- %-20s : %s", var_name, getattr(ozoevo_api.settings, var_name))


@app.on_event('startup')
def load_registry():
    app_log.info("registry initialization started")

    from ozoevo_api.db.robots import Registry
    Registry().refresh(timeout=ozoevo_api.settings.SCAN_TIMEOUT)

    app_log.info("registry initialization complete")


@app.on_event('startup')
def banner_close():
    app_log.info('startup sequence complete')
    app_log.info('-' * 80)


@app.on_event('shutdown')
def say_goodbye():
    app_log.info('-' * 80)
    app_log.info('Shutdown complete.')
    app_log.info('-' * 80)


def runserver():
    import uvicorn
    uvicorn.run(
        'ozoevo_api.main:app',
        host="0.0.0.0",
        port=ozoevo_api.settings.LISTEN_PORT,
        debug=ozoevo_api.settings.DEBUG,
    )


SYSTEMD_SERVICE_NAME = 'ozoevo-srvd'


def install_service():
    if os.getuid() != 0:
        sys.exit('Aborted : command requires root privileges')

    import pkg_resources
    import subprocess
    import shutil

    print(f'\nConfiguring service {SYSTEMD_SERVICE_NAME}...')

    print('- stopping service...')
    subprocess.run(['systemctl', 'stop', SYSTEMD_SERVICE_NAME])

    print('- installing service descriptor...')
    resource_package = 'ozoevo_api'
    resource_path = f'data/{SYSTEMD_SERVICE_NAME}.service'
    shutil.copy(pkg_resources.resource_filename(resource_package, resource_path), '/lib/systemd/system/')

    print('- enabling service...')
    subprocess.run(['systemctl', 'enable', SYSTEMD_SERVICE_NAME])
    print('- starting service...')
    subprocess.run(['systemctl', 'start', SYSTEMD_SERVICE_NAME])
    print('- service status check :')
    subprocess.run(['systemctl', 'status', SYSTEMD_SERVICE_NAME])

    print('\nDone.')


def uninstall_service():
    if os.getuid() != 0:
        sys.exit('Aborted : command requires root privileges')

    import subprocess

    print(f'\nRemoving service {SYSTEMD_SERVICE_NAME}...')

    print('- stopping service...')
    subprocess.run(['systemctl', 'stop', SYSTEMD_SERVICE_NAME])
    print('- disabling service...')
    subprocess.run(['systemctl', 'disable', SYSTEMD_SERVICE_NAME])
    print('- removing service descriptor...')
    os.remove(f'/lib/systemd/system/{SYSTEMD_SERVICE_NAME}.service')

    print('\nDone.')


def restart_service():
    if os.getuid() != 0:
        sys.exit('Aborted : command requires root privileges')

    import subprocess

    print(f'\nRestarting service {SYSTEMD_SERVICE_NAME}...')
    subprocess.run(['systemctl', 'restart', SYSTEMD_SERVICE_NAME])
    print('Service status check :')
    subprocess.run(['systemctl', 'status', SYSTEMD_SERVICE_NAME])

    print('\nDone.')


if __name__ == '__main__':
    # to be used in dev mode only
    import uvicorn
    uvicorn.run(
        'ozoevo_api.main:app',
        host="0.0.0.0",
        port=ozoevo_api.settings.LISTEN_PORT,
        reload=True,
        debug=True
    )
