import os

from dotenv import load_dotenv

__all__ = ['DEBUG', 'OFFLINE_MODE', 'SPEED_FACTOR', 'SPEED_MAX', 'SCAN_TIMEOUT']


def get_boolean_var(name: str, default=False) -> bool:
    return os.getenv(name, str(default)).lower() in ('1', 'true', 'yes', 'on')


def get_int_var(name: str, default=0) -> int:
    return int(os.getenv(name, str(default)))


def get_float_var(name: str, default=0.0) -> float:
    return float(os.getenv(name, str(default)))


load_dotenv()

DEBUG = get_boolean_var('OZOEVO_API_DEBUG')
OFFLINE_MODE = get_boolean_var('OZOEVO_API_OFFLINE')

SPEED_MAX = get_int_var('OZOEVO_API_SPEED_MAX', 3000)
SPEED_FACTOR = get_float_var('OZOEVO_API_SPEED_FACTOR', 1.0)

SCAN_TIMEOUT = get_int_var('OZOEVO_API_SCAN_TIMEOUT', 5)

LISTEN_PORT = get_int_var('OZOEVO_API_LISTEN_PORT', 8000)
