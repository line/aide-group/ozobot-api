import logging.config


logging.config.dictConfig({
    'version': 1,
    'formatters': {
        'console': {
            'format': '%(asctime)s [%(levelname).1s] %(name)-20s > %(message)s',
            'datefmt': '%H:%M:%S'
        }
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'level': logging.INFO,
            'formatter': 'console'
        }
    },
    'root': {
        'handlers': ['console'],
        'level': logging.INFO,
    },
    'loggers': {
        'ozobot': {
            'handlers': ['console'],
            'propagate': False,
        },
        'app': {
            'handlers': ['console'],
            'propagate': False,
        },
    }
})

app_log = logging.getLogger('app')
ozobot_log = logging.getLogger('ozobot')
