from ozoevo_api.models.robots import BTDevice

from ozoevo.bt import RobotFinder
from ozoevo.robot import OzoEvo

from ozoevo_api.log import app_log
from ozoevo_api.settings import OFFLINE_MODE

logger = app_log.getChild('registry')

OZOBOT_PREFIX = 'Ozo'


class ModelsError(Exception):
    pass


class DoesNotExist(ModelsError):
    def __init__(self, name: str):
        super().__init__(name)


class AlreadyExist(ModelsError):
    def __init__(self, name: str):
        super().__init__(name)


class Entry:
    _device: BTDevice
    _bot: OzoEvo = None

    def __init__(self, device: BTDevice):
        self._device = device

    @property
    def device(self) -> BTDevice:
        return self._device

    @property
    def bot(self) -> OzoEvo:
        if self._bot is None:
            dev = self._device
            logger.info('lazy creation of (%s, %s)', dev.device_name, dev.mac_addr)
            self._bot = OzoEvo(dev.mac_addr, offline=OFFLINE_MODE, logger=app_log.getChild('ozoevo'))
            self._bot.connect()

        return self._bot


class Registry(dict):
    instance = None
    finder = None

    def __new__(cls) -> 'Registry':
        if cls.instance is None:
            logger.info('creating registry instance')
            cls.instance = super().__new__(cls)     # type: Registry
            cls.finder = RobotFinder(OFFLINE_MODE, app_log.getChild('finder'))

        return cls.instance

    def get_robot(self, name: str) -> OzoEvo:
        return self[name].bot

    def register_robot(self, device: BTDevice, replace=False):
        if not device.device_name.startswith(OZOBOT_PREFIX):
            raise ValueError('not an Ozobot device')

        bot_name = device.device_name.lstrip(OZOBOT_PREFIX)
        if bot_name in self and not replace:
            return

        self[bot_name] = Entry(device=device)

    def refresh(self, timeout=10) -> dict:
        logger.info('refreshing robot directory...')
        self.finder.find_robots(timeout=timeout)

        # check already known ones for MAC address change
        count = 0
        for name, descr in ((n, d) for n, d in self.finder.items() if n in self):
            if descr.mac_addr != self[name].device.mac_addr:
                del self[name]
                count += 1
        logger.info('- %d MAC address change(s)', count)

        # add new robots
        count = -count      # take in account robots which MAC has changed
        for name, descr in ((n, d) for n, d in self.finder.items() if n not in self):
            self[name] = Entry(BTDevice(device_name=descr.device_name, mac_addr=descr.mac_addr))
            count += 1
        logger.info('- %d new bot(s) discovered', count)

        # delete gone ones
        count = 0
        for name in [n for n in self if n not in self.finder]:
            del self[name]
            count += 1
        logger.info('- %d bot(s) gone', count)

        count = len(self)
        if count:
            logger.info("registry contains %d bot(s) : %s", count, ', '.join(self))
        else:
            logger.warning("registry is empty")

        return dict(self.finder.items())


def get_registry() -> Registry:
    return Registry()
