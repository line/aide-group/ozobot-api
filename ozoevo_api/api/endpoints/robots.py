from typing import List, Union

from fastapi import APIRouter, Depends, HTTPException

from starlette.status import HTTP_204_NO_CONTENT

from ozoevo.robot import OzoEvo, LedSelector

from ozoevo_api.models.robots import RobotDescriptor, LEDsSettings, DriveSettings, SpinSettings
from ozoevo_api.db.robots import get_registry, Registry
from ozoevo_api.settings import SPEED_FACTOR, SCAN_TIMEOUT


router = APIRouter()


@router.get("/",
            summary='List known robots',
            response_model=List[Union[str, RobotDescriptor]],
            operation_id='list_robots'
            )
def list_robots(long: bool = False,
                rescan: bool = False,
                timeout: int = SCAN_TIMEOUT,
                reg: Registry = Depends(get_registry),
                ):
    """ Returns all the robots within reach during last scan.
    """
    if rescan:
        reg.refresh(timeout=timeout)

    if long:
        return [RobotDescriptor(name=n, device=v.device) for n, v in reg.items()]
    else:
        return list(reg.keys())


@router.get("/{name}",
            summary="Retrieve the descriptor of a given robot",
            response_model=RobotDescriptor,
            operation_id='get_robot_descriptor'
            )
def retrieve_robot_descriptor(name: str, reg: Registry = Depends(get_registry)):
    """ Retrieves information about the robot, including Bluetooth configuration.
    """
    try:
        entry = reg[name]
    except KeyError:
        raise HTTPException(status_code=404, detail={
            'description': 'Robot not found',
            'name': name
        })

    return RobotDescriptor(name=name, device=entry.device)


def get_robot(name: str, reg: Registry = Depends(get_registry)) -> OzoEvo:
    try:
        return reg[name].bot
    except KeyError:
        raise HTTPException(status_code=404, detail={
            'description': 'Robot not found',
            'name': name
        })


@router.put("/{name}/drive",
            summary="Control the moves of a given robot",
            response_model=DriveSettings,
            response_description="the full settings used, included defaulted values",
            operation_id='drive_robot'
            )
def drive_robot(settings: DriveSettings, robot: OzoEvo = Depends(get_robot)):
    """ Makes the robot move by setting motor speeds and move duration.
    """
    robot.drive(
        left_speed=int(settings.left_speed * SPEED_FACTOR),
        right_speed=int(settings.right_speed * SPEED_FACTOR),
        secs=settings.delay,
        wait=settings.wait
    )
    return settings


@router.put("/{name}/spin",
            summary="Make a given robot spin on itself",
            response_model=SpinSettings,
            response_description="the full settings used, included defaulted values",
            operation_id='spin_robot'
            )
def drive_robot(settings: SpinSettings, robot: OzoEvo = Depends(get_robot)):
    """ Makes the robot move by setting motor speeds and move duration.
    """
    robot.spin(
        speed=int(settings.speed * SPEED_FACTOR),
        secs=settings.delay,
        wait=settings.wait
    )
    return settings


@router.put("/{name}/leds",
            summary="Control the LEDs of a given robot",
            response_model=LEDsSettings,
            response_description="the full settings used, included defaulted values",
            operation_id='set_robot_leds'
            )
def set_robot_leds(settings: LEDsSettings, robot: OzoEvo = Depends(get_robot)):
    """ Change the state and color of one or more LEDs of the robot.

    The targeted LEDs are specified by the list of their symbolic names, among:
    `top`, `left`, `center_left`, `center`, `center_right`, `right`, `rear`

    The special symbols `all` or `*` can be used as shorthands for the complete list.
    """
    leds = [LedSelector[led_name] for led_name in settings.leds]

    robot.set_leds(leds, settings.color.red, settings.color.green, settings.color.blue)
    return settings


@router.put("/{name}/leds/off",
            summary="Turn off all LEDS of a given robot",
            status_code=HTTP_204_NO_CONTENT,
            operation_id='robot_leds_off'
            )
def turn_robot_leds_off(robot: OzoEvo = Depends(get_robot)):
    """ Turn all robot LEDs off.
    """
    robot.leds_off(LedSelector.all())
