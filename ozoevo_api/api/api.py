from fastapi import APIRouter

from ozoevo_api.api.endpoints import robots

api_router = APIRouter()

api_router.include_router(robots.router, prefix="/robots", tags=["robots"])
