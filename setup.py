from setuptools import setup, find_packages


setup(
    name='ozoevo-api',
    description='REST API server for controlling an OzoBot Evo',
    version='0.1.0',
    use_scm_version=True,
    setup_requires=['setuptools_scm'],
    packages=find_packages(),
    package_data={
        'ozoevo_api': ['data/*']
    },
    url='',
    license='',
    author='Eric Pascual',
    author_email='',
    python_requires='>=3.6',
    install_requires=[
        'colorama',
        'email-validator',      # not used, but avoids intrusive warnings
        'fastapi',
        'python-dotenv',
        'ujson',
        'uvicorn',
    ],
    entry_points={
        'console_scripts': [
            'ozoevo-srvd=ozoevo_api.main:runserver',
            'ozoevo-install-service=ozoevo_api.systemd:install_service',
            'ozoevo-uninstall-service=ozoevo_api.systemd:uninstall_service',
            'ozoevo-restart-service=ozoevo_api.systemd:restart_service',
        ]
    },
)
